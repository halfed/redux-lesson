import * as actionTypes from './actionTypes';

export const saveResult = (res) => {
  return {
    type: actionTypes.STORE_RESULT,
    result: res
  }
}

export const storeResult = (value) => {
  return function (dispatch, getState) {
    setTimeout(() => {
      ///const oldCounter = getState().ctr.counter;
      //console.log(oldCounter);
      dispatch(saveResult(value));
    }, 2000);
  }

  /*return {
    type: STORE_RESULT,
    result: value
  }*/
};

export const deleteResult = (value) => {
  return {
    type: actionTypes.DELETE_RESULT,
    resultElId: value
  }
};